<?php
/**
 * @file
 * Apply current comment configuration to all existing comments on nodes.
 */

define('COMMENT_BULK_ACTION_CHUNK_SIZE', 5);
/**
 * Implements hook_form_FORM_ID_alter().
 */
function comment_bulk_action_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  // Extra checkbox for updating new.
  $form['comment']['comment_action_all_node'] = array(
    '#type' => 'checkbox',
    '#title' => t("Update comment configuration for existing nodes"),
    '#description' => t("Run batch to update comment configuration for all existing nodes."),
  );
  $form['#submit'][] = 'comment_bulk_action_form_submit';
}

/**
 * Implements hook_help().
 */
function comment_bulk_action_help($path, $arg) {
  $help = NULL;
  switch ($path) {
    case "admin/help#comment_bulk_action":
      $help = t("<p>This module provide option on content type edit form,
      under comment section,
      where Administrator can run batch to apply
      new comment configuration for existing nodes if required.
      </p>
      <strong>HOW TO USE</strong>
      <ol>
      <li>Logged in as \"Administrator\" OR user account who is having
          access to edit/update content type configuration.</li>
      <li>Travel to
      \"Structure » Content Types » Any content type EDIT page\".</li>
      <li>Inside vertical tab \"Comment settings\" look for checkbox option
       with title \"Update comment configuration for existing nodes\".</li>
      </ol>");
      break;
  }
  return "<p> {$help} </p>";
}

/**
 * Batch process if user want to update comment configuration for existing node.
 */
function comment_bulk_action_form_submit(&$form, &$form_state) {
  $node_form_input = $form_state['values'];
  $node_type = $node_form_input['type'];
  if (1 == $node_form_input['comment_action_all_node']) {
    // Run batch to updated comment configuration for node.
    $operations = array();
    $node_list = _comment_bulk_action_fetch_nodes($node_type);
    $chunks_node_list = array_chunk($node_list, COMMENT_BULK_ACTION_CHUNK_SIZE, TRUE);
    $operation_count = 0;
    $comment_cofiguration = $node_form_input['comment'];
    foreach ($chunks_node_list as $node_ids) {
      $operations[] = array(
        'comment_bulk_action_update_node_comment_configuration',
        array(
          $node_ids,
          $comment_cofiguration,
          t('(Operation @operation)', array('@operation' => $operation_count++)),
        ),
      );
    }
    $batch = array(
      'operations' => $operations,
      'title' => t('Processing node comment configuration'),
      'init_message' => t('Node comment configuration update is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Node comment configuration has encountered an error.'),
      'finished' => 'comment_bulk_action_finished_update_node_comment_configuration',
    );
    batch_set($batch);
  }
}

/**
 * Fetch all node of selected type, to build batch operation.
 *
 * @param string $node_type
 *   List of all nodes.
 */
function _comment_bulk_action_fetch_nodes($node_type = NULL) {
  $node_list = array();
  if (isset($node_type)) {
    $node_list = db_query("SELECT nid FROM {node} WHERE type = :node_type ORDER BY node.nid ASC", array(':node_type' => $node_type))->fetchAllAssoc("nid");
  }
  return $node_list;
}

/**
 * Actual node comment configuration update.
 */
function comment_bulk_action_update_node_comment_configuration($nids, $comment_cofiguration, $operation_status, &$context) {
  $nodes = node_load_multiple(array_keys($nids));
  foreach ($nodes as $node) {
    $context['message'] = t('Loading node "@title"', array('@title' => $node->title));
    $node->comment = $comment_cofiguration;
    node_save($node);
  }
  _comment_bulk_action_batch_update_http_requests();
}

/**
 * Count the number HTTP request needed to complete batch operation.
 */
function _comment_bulk_action_batch_update_http_requests() {
  $_SESSION['http_request_count']++;
}

/**
 * Get count the number HTTP request needed to complete batch operation.
 */
function _comment_bulk_action_batch_get_http_requests() {
  return !empty($_SESSION['http_request_count']) ? $_SESSION['http_request_count'] : 0;
}

/**
 * Batch finished function to display final result for batch processing.
 */
function comment_bulk_action_finished_update_node_comment_configuration($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    drupal_set_message(t('Processed in @requests HTTP requests.', array('@count' => count($results), '@requests' => _comment_bulk_action_batch_get_http_requests())));
    drupal_set_message(t('All existing comments on nodes are updated successfully with current comment configuration.'));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args',
      array(
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
        )
      )
    );
  }
  unset($_SESSION['http_request_count']);
}
