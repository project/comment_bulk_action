
-- INTRODUCTION --

  Module provide option to update comment configuration
  for existing nodes(if any exist).
  When configuration for any content type updated for comments,
  its applicable only for future nodes.
  There is no option/module which can take care
  And confirm that latest comments configuration set to existing/old nodes.
  This module provide option on content type edit form,
  under comment setting section,
  where Administrator can run batch to apply
  new comment configuration for existing nodes if required.

-- How to use --

 1) Logged in as "Administrator" OR user account who is having
    access to edit/update content type configuration.
 2) Travel to "Structure » Content Types » Any content type EDIT page".
 3) Inside vertical tab "Comment settings" look for checkbox option with title
    "Update comment configuration for existing nodes".


For a full description of the module, visit the project sandbox page:
  https://www.drupal.org/sandbox/tusharbodke/2551319

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

None.

-- CUSTOMIZATION --

None.

-- TROUBLESHOOTING --

None.

-- FAQ --

None.

-- CONTACT --

Current maintainers:
 Tushar Bodake (tusharbodke) - https://www.drupal.org/user/759838


This project has been sponsored by:
  Faichi Solution Pvt Ltd, India.
  Specialized in consulting and planning of Drupal powered sites,
  FAICHI SOLUTION
  offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.faichi.com for more information.
